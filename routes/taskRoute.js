const express = require("express");

const router = express.Router();

const {
  getAllTasks,
  createTask,
  deleteTask,
  getTask,
  updateTask,
} = require("../controllers/taskController");

// Get all task
router.get("/", getAllTasks);

// Get single task
router.get("/:id", getTask);

// Create new task
router.post("/", createTask);

// Delete specific task
router.delete("/:id", deleteTask);

// Update specific task
router.patch("/:id", updateTask);

// export router
module.exports = router;

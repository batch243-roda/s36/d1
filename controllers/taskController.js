const Task = require("../models/taskModel");
const mongoose = require("mongoose");

// Get all task
const getAllTasks = async (req, res) => {
  const tasks = await Task.find({});

  res.status(200).send(tasks);
};

// Get single task
const getTask = async (req, res) => {
  const { id } = req.params;

  // reverse the isValid function so that if id is not valid or false i make it to true to enter the if statement
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).send({ error: "No such task" });
  }

  const task = await Task.findById(id);

  if (!task) {
    return res.status(404).send({ error: "No such task" });
  }

  res.status(200).json(task);
};

// Create new task
const createTask = async (req, res) => {
  const { name } = req.body;

  // add doc to db
  try {
    const tasks = await Task.create({ name });
    res.status(200).send(tasks);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};

// Delete specific task
const deleteTask = async (req, res) => {
  const { id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).send({ error: "No such task" });
  }

  const task = await Task.findOneAndDelete({ _id: id });

  if (!task) {
    return res.status(404).send({ error: "No such task" });
  }

  res.status(200).send(task);
};

// Update specific task
const updateTask = async (req, res) => {
  const { id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).send({ error: "No such task" });
  }

  const task = await Task.findByIdAndUpdate(
    { _id: id },
    {
      ...req.body,
    }
  );

  if (!task) {
    return res.status(404).send({ error: "No such task" });
  }

  res.status(200).send(task);
};

module.exports = { getAllTasks, createTask, deleteTask, getTask, updateTask };
